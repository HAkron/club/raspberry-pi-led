# HAkron Club Project 1
## Raspberry Pi LED / Button Crossover

WHAT'S UP, HACKERS - The first multi-week project of the semester is HERE. Let's look at a run-down:
* The end product will consist of a Raspberry Pi Zero W connected to a button and an LED. The button will control an "LED" on a webserver (also being run by the pi), and the button on the webserver will control the hardware LED. The ol' switcharoo!
* The software consists of: 
    * **Node.js** - Javascript runtime-environment with a huge, easy to use package library
    * 2 node modules (**onoff** and **socket.io**, we'll talk about those later)
    * **ngrok** - forwards traffic from a local server to the internet! Yay tunneling!
* The hardware consists of: A Raspberry Pi Zero W, a breadboard, some wires, an LED, a button, and 2 resistors (each 1k ohms)


---
## Table of Contents: 
### [Week 1 (Jan 29th): Downloads and Code Editing](#week-1-downloads-and-code-editing)
### [Week 2 (Feb 5th): Doing Stuff on the Raspberry Pi Zero W](#week-2-doing-stuff-on-the-raspberry-pi-zero-w) 
### [Week 3 (Feb 12th): Mashing them together](#week-3-mashing-them-together) 

---
### Week 1: Downloads and Code Editing
Here's the software that we'll be using on our own computers in this workshop. It would be ideal for you to use these since this is what we'll be using to teach you (but feel free to use something else if you're more comfortable with it):
* [Visual Studio Code](https://code.visualstudio.com/download) - We'll be writing code on our own computers since typing in default Raspberry Pi text editors can be a pain. If you want to use any other text editor, go ahead!
* Windows only: [Git Bash](https://git-scm.com/downloads) - Since we're going to write code on our own computers, we need to transfer it to the Pi somehow. We'll use the `scp` command to do that, and the `ssh` command to connect to our pi. If you're on Linux or OSX, you won't need to download anything! Your terminal will work just fine. 

### Creating an Ngrok account
To be able to SSH and pass the webserver through the university wifi, we need to use a tool called ngrok. Normally, you don't need to make an account for this. But since we'll be SSH'ing through ngrok, ngrok wants us to make a free account. No big deal! 

Go to [ngrok's website](https://ngrok.com) and click "Sign Up" in the top right. Sign up any way you wish, then the important part comes when it sends you back to your dashboard. On step 3 ("Connect your account") of the 4 installation steps, we need the long authtoken it shows in the command. **Copy this authtoken and send it to one of the HAkron officers with your name** so we can do some behind the scenes magic to get your pi set up with the university wifi. 

(This process involves plugging a monitor and keyboard into the pi's, and hand-typing that command with the authtoken that you see in step 3. If we had one monitor and one keyboard, had 10+ people do this one at a time, and it took 5 minutes per person, that's 50 minutes of workshop time that could be spent doing something more useful. So we'll just do this behind the scenes between meetings! If you have questions or concerns about this, talk to one of the HAkron officers)

### Clone/Download the program files
To get started, you'll need to download the base code we have on [our GitLab repository](https://gitlab.com/HAkron/club/raspberry-pi-led). We'll be adding some meaningful code to these files to make them functional.

[![N|Solid](https://gitlab.com/HAkron/club/raspberry-pi-led/raw/master/imgs/git-clone-download-pi-repo.jpg)](https://gitlab.com/HAkron/club/raspberry-pi-led)

Open *Terminal* or *Git Bash* 
Let's first `cd` (**c**hange **d**irectory) to the place you want to store this file: 

`cd Documents`

(REVIEW: If you don't know where you're at in the file tree, type `ls` for to see a list of files in the directory(folder) you're currently in. Type `cd ..` and if you want to go back a directory.)

Let's clone the project folder into this Documents folder using `git clone`:

`git clone https://gitlab.com/HAkron/club/raspberry-pi-led.git`

And now you have the Gitlab repository locally on your machine! 

#### Editing the Code!

We will be doing this *LIVE* during our meeting 1/29/19, but we made up a little document showing you all the changes we'll be making. Find el document [here](https://gitlab.com/HAkron/club/raspberry-pi-led/blob/master/codingSteps.pdf).

---

### Week 2: Doing Stuff on the Raspberry Pi Zero W
Before we turn anything on or plug anything in, let's hook up our LED, Button, and resistors to the breadboard. We've already soldered the 4 Necessary wires to the pi, color coded like so:
* RED - 3.3v
* BLACK - Ground
* GREEN - GPIO5
* WHITE - GPIO25
1. On the breadboard, hook up the Red and Black wires to the positive and negative side rails respectively. 
2. Hook up the square button so that is bridges the trench 
3. Connect the top-left terminal of the button to the positive side rail,
4. the bottom right terminal to GPIO5, 
5. and a resistor (both are the same value) from the bottom left terminal to the ground rail.
6. Put the shorter negative pin of the LED into the groud rail, and the longer positive end into an empty row in the middle somewhere. 
7. Put one end of the other resistor into the same row as the LED, 
8. and the other end into the same row as GPIO25

That's all for the breadboard connections - if you got lost during this 8 step process, a refresher on [how breadboards work](https://gitlab.com/HAkron/club/raspberry-pi-led/raw/master/imgs/breadboard.png) would be helpful.  

Before we dive into the pi, lets cover the things we're going to install:
* **Node.js** - This will be our webserver, and serve the file to make our webpage
* Node package - **onoff** - This allows us  to access the GPIO pins on the pi to interact with the button and LED
* Node package - **socket.io** - This opens specific 'sockets' so we can have bi-directional real time communication between our webserver and the person viewing the webpage.


First, we need to connect our raspberry pi over the network by using ssh (**S**ecure **SH**ell). This way, we can use a terminal on it just as if we we're typing on a keyboard plugged into it. 

Make sure your pi is plugged in and being powered, then go back to [ngrok's website](https://ngrok.com) that we went to last week. Click on "Status" in the side tab. If you don't see anything show up, try waiting a minute or two after you turn on your pi.

You should see a TCP tunnel currently running on the pi with an ngrok url. We're going to use that url to SSH into the pi. The url will look like this:
**0.tcp.ngrok.io:[port]**

Using Git Bash or a terminal, let's ssh into our pi's. The command is:
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`ssh -p [port] pi@0.tcp.ngrok.io`
Enter the password `raspberry` when it prompts you for one.

Once connected, we need to set up and install some things. First, lets downlaod Node.js

1. To download the installer on the raspberry pi, type 
`wget https://nodejs.org/dist/v10.15.1/node-v10.15.1-linux-armv6l.tar.xz`

2. To unzip what we just downloaded, type
`tar -xf node-v10.15.1-linux-armv6l.tar.xz`

3. Change directories into that unzipped folder,
`cd node-v10.15.1-linux-armv6l`

4. Then copy it all to your root directory with 
`sudo cp -R * /`

5. To test that it installed correctly, we can check the version by typing
`node -v`

Now that node is installed, we can install those two packages we used in our code: *onoff* and *socket.io*
`npm install onoff`
`npm install socket.io`

That's all we need to install! Type `exit` in your terminal to close the ssh connection. Next week, we're going to move the files over, start up the webserver, and press some buttons!

---

### WEEK 3: MASHING THEM TOGETHER

[![N|Solid](https://gitlab.com/HAkron/club/raspberry-pi-led/raw/master/imgs/mashed.gif)](https://tenor.com)


The only things we need to do this week are transfer the two code files over to the pi, start up the webserver, and start ngrok on that port.

We'll transfer the two files over using the `scp` command in Git Bash. We'll do this a similar way to how we ssh'd, using the ngrok url. So open up git bash, navigate to the directory that the files we wrote during Week 1 are in, and then type the following:
`scp -P [port] index.html pi@0.tcp.ngrok.io:~/Documents/index.html`
`scp -P [port] webserver.js pi@0.tcp.ngrok.io:~/Documents/webserver.js`
It'll ask you for a password again. Just like last week, the default password is `raspberry`.

Neat! Now we need to ssh back into the pi to start up the webserver. Remember we're still ssh'ing over port 443. Just like before, we can use
`ssh -p [port] pi@0.tcp.ngrok.io` and again enter `raspberry` as the password.

Now that we're in, let's make sure those files we transfered over are there. You should be in your home directory on the pi right after you ssh'd in, so type
`cd Documents`, then `ls` to view everything in the documents file. Those two files, along with ngrok should pop up. If that's the case, then you're ready to whip up a webserver. 

To start our webserver in the background, type 
`nohup node webserver.js &`
The `nohup` command runs it in the background and lets us keep using the terminal. (If you need to stop/restart this process to edit code or troubleshoot, type `pkill -f node` to kill all running node processes)

We already have a tcp tunnel running on ngrok, and we can't have multiple running at once. So we'll have to stop the TCP tunnel and start a new HTTP tunnel in its place. This will stop us from being able to ssh, so it'll kill the connection. 

Luckily, we wrote a script that does this exact thing for us! Make sure you're in the Documents directory, and type:
`sudo ./switcharoo.sh`

If everything is working, you'll get kicked out of ssh and should see two HTTP tunnels on your [ngrok dashboard](https://dashboard.ngrok.com/status) instead of one TCP tunnel. If you visit one of these new HTTP URLs, you should see your website! You can even do it on your phone if you want. 

If not, just unplug your pi, wait a few seconds, then plug it back in. The TCP script kicks in on reboot, allowing you to SSH back in and try again.


Once you're on the website, the checkbox can be clicked to control the hardware LED on the pi, and the hardware button can be pressed to control the software 'LED' on the website! Ta'Da!

If you participated in all the workshops and completed a working product, congrats! You get to keep those parts. We hope you had fun, but more importantly, learned something!

### Here's some documentation to the great tools we used:
* [ngrok docs](https://ngrok.com/docs)
* [node.js docs](https://nodejs.org/en/docs/)
* [Project](https://www.w3schools.com/nodejs/nodejs_raspberrypi_webserver_websocket.asp) that this one was adapted from